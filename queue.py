# -*- coding: utf-8 -*-

from stacks import *

"""
Queues
"""
condutores = Queue()
percursoNorte = Stack()
percursoSul = Stack()

#Popular a queue de condutores
rui = Pessoa('Rui','123456789')
ana = Pessoa('Ana','123456789')
condutores.enqueue(rui)
condutores.enqueue(ana)

#Criar cidades
setubal = Cidade('Setúbal',20,1)
beja = Cidade('Beja',10,2)
evora = Cidade('Évora',4,3)
lisboa = Cidade('Lisboa',1,4)
porto = Cidade('Porto',20,1)
braga = Cidade('Braga',10,2)
aveiro = Cidade('Aveiro',5,3)
coimbra = Cidade('Coimbra',1,4)

#Percurso Sul
percursoSul.push(setubal)
percursoSul.push(beja)
percursoSul.push(evora)
percursoSul.push(lisboa)

#Percurso Norte
percursoNorte.push(porto)
percursoNorte.push(braga)
percursoNorte.push(aveiro)
percursoNorte.push(coimbra)

for condutor in range(condutores.size()):
    print(condutores.dequeue())
    if condutor == 1:
        for cidade in range(percursoNorte.size()):
            print(percursoNorte.pop())
    else:
        for cidade in range(percursoSul.size()):
            print(percursoSul.pop())

print('\n ----------------- Com input ------------- \n')

flag = True
while flag:
    nome = raw_input("Coloque o nome do condutor \n")
    ccond = raw_input("Coloque o número de carta \n")
    condutores.enqueue(Pessoa(nome,ccond))
    question = input("Deseja adicionar mais? [1]Não - [2]Sim\n")
    if question == 1:
        flag = False

add_percurso = True
while add_percurso:
    choose_2 = input("Quer inserir no percurso norte ou sul? [1]Norte - [2]Sul - [0]Sair \n")
    if choose_2 == 1:
        nome_cidade = raw_input("O nome da cidade: \n")
        pop = raw_input("População da cidade: \n")
        km = raw_input("Km2 da cidade: \n")
        percursoNorte.push(Cidade(nome_cidade,pop,km))
        print("Adiciondo com sucesso")
    elif choose_2 == 2:
        nome_cidade = raw_input("O nome da cidade: \n")
        pop = raw_input("População da cidade: \n")
        km = raw_input("Km2 da cidade: \n")
        percursoSul.push(Cidade(nome_cidade,pop,km))
        print("Adiciondo com sucesso")
    else:
        add_percurso = False

for condutor in range(condutores.size()):
    print("\nCondutor")
    print(condutores.dequeue())
    if condutor == 0:
        print("\nPercurso Norte:")
        if percursoNorte.isEmpty():
            print("Está vazio o percurso!")
        else:
            for cidade in range(percursoNorte.size()):
                print(percursoNorte.pop())
    else:
        print("\nPercurso Sul:")
        if percursoSul.isEmpty():
            print("Está vazio o percurso!")
        else:
            for cidade in range(percursoSul.size()):
                print(percursoSul.pop())
