# -*- coding: utf-8 -*-

import random
import timeit

class listSorting:
    def __init__(self,alist):
        self.listaOriginal = alist
        self.alist = alist

    def __str__(self):
        return('A lista ordenada é {}'.format(self.alist))

    ##############
    ##Quick Sort##
    ##############

    def quickSort(self):
       self.quickSortHelper(self.alist,0,len(alist)-1)

    def quickSortHelper(self,alist,first,last):
       if first<last:

           splitpoint = self.partition(self.alist,first,last)

           self.quickSortHelper(self.alist,first,splitpoint-1)
           self.quickSortHelper(self.alist,splitpoint+1,last)

    def partition(self,alist,first,last):
       pivotvalue = self.alist[first]

       leftmark = first+1
       rightmark = last

       done = False
       while not done:

           while leftmark <= rightmark and self.alist[leftmark] <= pivotvalue:
               leftmark = leftmark + 1

           while self.alist[rightmark] >= pivotvalue and rightmark >= leftmark:
               rightmark = rightmark -1

           if rightmark < leftmark:
               done = True
           else:
               temp = alist[leftmark]
               self.alist[leftmark] = alist[rightmark]
               self.alist[rightmark] = temp

       temp = alist[first]
       self.alist[first] = self.alist[rightmark]
       self.alist[rightmark] = temp


       return rightmark

    ##############
    ##Merge Sort##
    ##############

    def mergeSort(self):
        if len(self.alist)>1:
            mid = len(self.alist)//2
            lefthalf = self.alist[:mid]
            righthalf = self.alist[mid:]

            lefthalfInst = listSorting(lefthalf)
            lefthalfInst.mergeSort()
            righthalfInst = listSorting(righthalf)
            righthalfInst.mergeSort()
            i=0
            j=0
            k=0
            while i < len(lefthalfInst.alist) and j < len(righthalfInst.alist):
                if lefthalfInst.alist[i] < righthalfInst.alist[j]:
                    self.alist[k]=lefthalfInst.alist[i]
                    i=i+1
                else:
                    self.alist[k]=righthalfInst.alist[j]
                    j=j+1
                k=k+1

            while i < len(lefthalfInst.alist):
                self.alist[k]=lefthalfInst.alist[i]
                i=i+1
                k=k+1

            while j < len(righthalfInst.alist):
                self.alist[k]=righthalfInst.alist[j]
                j=j+1
                k=k+1

if __name__ == '__main__':
    def profile(f):
        """ Calcula o tempo gasto pela função f(x)"""
        def inner(*x):
            tempo = time.time()
            res = f(*x)
            tfinal= time.time() - tempo
            print ('Tempo gasto por {}: {} miliseg.'.format(f.__name__, tfinal*1000))
            return res
        return inner

    print('\n')
    print('-'*100)

    alist = random.sample(range(100), 10)
    print('A lista original utilizando MergeSort é {}'.format(alist))
    listSorting(alist).mergeSort()
    print(listSorting(alist))
    
    print('-'*100)

    alist = random.sample(range(100), 10)
    print('A lista original utilizando QuickSort é {}'.format(alist))
    start_time = timeit.default_timer()
    listSorting(alist).quickSort()
    elapsed = timeit.default_timer() - start_time
    print(listSorting(alist))
    print('Tempo de execução: {}'.format(elapsed))

    print('-'*100)
    print('\n')
