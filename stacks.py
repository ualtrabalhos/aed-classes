# -*- coding: utf-8 -*-

class Stack:
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def push(self,item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def peek(self):
        return self.items[len(self.items)-1]

    def size(self):
        return len(self.items)

class Queue:
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def enqueue(self,item):
        self.items.insert(0,item)

    def dequeue(self):
        return self.items.pop()

    def peek(self):
        return self.items[len(self.items)-1]

    def size(self):
        return len(self.items)

class Cidade:
    def __init__(self,nome,populacao,km2):
        self.nome = nome
        self.populacao = populacao
        self.km2 = km2

    def __str__(self):
        return ("\n {}, População: {}, km2: {}".format(self.nome,self.populacao,self.km2))

class Pessoa:
    def __init__(self,nome,ccond):
        self.nome = nome
        self.ccond = ccond

    def __str__(self):
        return ("\n {}, carta de condução: {}".format(self.nome,self.ccond))

if __name__ == '__main__':
    #Criação de objetos
    setubal = Cidade('Setúbal',20,1)
    beja = Cidade('Beja',10,2)
    evora = Cidade('Évora',4,3)
    lisboa = Cidade('Lisboa',1,4)

    #Atribuição da classe a uma variavel para que possa ser instanciada por objeto
    percurso = Stack()

    #Preencher o stack
    percurso.push(lisboa)
    percurso.push(setubal)
    percurso.push(evora)
    percurso.push(beja)

    #Print do percurso inicial
    print("\n O percurso inverso de Lisboa a Beja é:")
    for cidade in range(percurso.size()):
        print(percurso.pop())
    print("\n")

    #Preenchemos outra vez o stack.. nesta altura está vazio porque esvaziamos acima
    percurso.push(lisboa)
    percurso.push(setubal)
    percurso.push(evora)
    percurso.push(beja)

    #Input para filtrar
    input = int(input("Introduza o número de população para filtrar as cidades. \n"))

    #Helper func para fazer output do header só uma vez
    runned = False
    def output_header(exists):
        if exists == True:
            print("As cidades que têm uma população inferior a {} são:".format(input))
        else:
            print("Não existem cidades que têm uma população inferior a {}".format(input))

    #Itera sobre o tamanho do stack
    for populacao in range(percurso.size()):
        if percurso.peek().populacao >= input:
            percurso.pop() #não corresponde ao que estamos à procura então sai da pilha e não faz print
            if percurso.size() == 0: #se chegarmos ao 0 é porque não encontramos nada, então faz print da mensagem correspondente
                output_header(False)
        else:
            while runned == False: #só queremos que o header apareça uma vez, sendo assim defini uma variavel para saber quando já correu.
                output_header(True)
                runned = True
            print(percurso.pop()) #faz print dos objetos da pilha que nos interessam

## TODO ###
"""
6.	Generalize este programa particular, de forma a que o utilizador possa definir o percurso com a sequência de cidades que desejar.
"""
