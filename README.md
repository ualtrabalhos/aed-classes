# Algoritmia e Estruturas de Dados

**Como utilizar?**

1. Instalar git -> https://git-scm.com/downloads
2. `git clone git@bitbucket.org:ualtrabalhos/algoritmia-e-estruturas-de-dados-geral.git`
3. Executar programa no IDE favorito.

**Exercícios de aula**

1. Ex. sobre classes com a criação de classes Contacto, Aluno e Professor. Criação de métodos na classe e evocação.
2. Ex. sobre Stacks, criação da classe Stack e Cidade. Manipulação de Stacks.
