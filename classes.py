import datetime
from time import strftime

class Contacto:
    def __init__(self,nome,email,telefone,aniversario):
        self.nome = nome
        self.email = email
        self.telefone = telefone
        self.aniversario = aniversario

    def __str__(self):
        return('Esta classe é {} e o objeto atual é {}'.format(self.__class__.__name__, self.nome))

class Professor(Contacto):
    def __init__(self,grau,disciplinas):
        super().__init__(nome,email,telefone,aniversario)
        self.grau = grau
        self.disciplinas = disciplinas

    def __str__(self):
        return('Esta classe é {} e o objeto atual é {}'.format(self.__class__.__name__, self.nome))

class Aluno(Contacto):
    def __init__(self,nome,email,telefone,aniversario,numAluno,curso,ano,turma,notas):
        #Herança da classe Contacto
        super().__init__(nome,email,telefone,aniversario)
        #Variaveis desta classe
        self.numAluno = numAluno
        self.curso = curso
        self.ano = ano
        self.turma = turma
        self.notas = notas

    def __str__(self):
        if self.dateValidation() == False: #validação da data dentro da classe
            return('\n Olá {} \n Aluno nº: {} \n Email: {} \n Telefone: {} \n Aniversário: {}, \n Curso: {}, Ano: {}, Turma: {}, \n Notas: {} \n Media: {} \n'
            .format(self.nome,self.numAluno,self.email,self.telefone,self.aniversario,self.curso,self.ano,self.turma,self.notas,self.media()))
        elif self.dateValidation() == True:
            return 'O aluno {} tem a data incorrecta, deve ser YYYY-MM-DD'.format(self.nome)

    #Metodos da classe
    def media(self):
        soma = 0
        tamanhoLista = len(self.notas)
        for nota in range(tamanhoLista):
            soma = soma + self.notas[nota]
        media = soma / tamanhoLista
        return round(media,2)
    #Validação da data
    def dateValidation(self):
        try:
            datetime.datetime.strptime(self.aniversario, '%Y/%m/%d')
            return False
        except ValueError:
            return True

if __name__ == "__main__":
    #Inicializar objetos
    Joao = Aluno('João','jony@gmail.com',913049574,'2018/03/12',1,'Eng. Inf.',2018,"B",[10,12,12])
    Jose = Aluno('José','jose@gmail.com',912032123,'2018/03/12',2,'Eng. Inf.',2016,"B",[20,18,15])
    Joana = Aluno('Joana','joana@gmail.com',2132423,'2018/04/12',3,'Eng. Inf.',2015,"B",[5,10,15])
    Vasco = Aluno('Vasco','Vasco@gmail.com',2132423,'2018/03/12',3,'Eng. Inf.',2015,"B",[5,10,15])
    #Append de objetos a lista
    listaAl = []
    listaAl.append(Joao)
    listaAl.append(Jose)
    listaAl.append(Joana)
    listaAl.append(Vasco)
    #Dividir por turmas
    metade = int(len(listaAl) / 2)
    turmaA = listaAl[:metade]
    turmaB = listaAl[metade:]
    #Print alunos por turma
    print("Aluno turma A")
    for aluno in turmaA:
        print(aluno)
    print("Aluno turma B")
    for aluno in turmaB:
        print(aluno)
